#!/bin/bash

set -e

if [ $# -ne 1 ]; then
    echo "usage: $0 name"
    exit 1
fi

RUN_DIR=${RUN_DIR:-/docker/run}
NAME=$1

/bin/mkdir -p $RUN_DIR
exec /usr/bin/docker inspect -f '{{.State.Pid}}' $NAME >/docker/run/$NAME.pid
