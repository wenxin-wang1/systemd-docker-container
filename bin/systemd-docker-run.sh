#!/bin/bash

set -e

if [ $# -lt 2 ]; then
    echo "usage: OPTS=... $0 name service_name run_cmd..."
    exit 1
fi

assert_non_empty() {
    if [ z"$2" == z ]; then
        echo $1
        exit 1
    fi
}

assert_var_non_empty() {
    assert_non_empty "$1 is empty" "${!1}"
}

CGROUP_PARENT=$(grep 'name=' /proc/$$/cgroup | head -n1 | cut -d':' -f3)
assert_var_non_empty CGROUP_PARENT

_NAME=$1
_SERVICE=$2

RUN_ENV_FILE=/docker/run-envs/$_NAME
if [ ! -r $RUN_ENV_FILE ]; then
    echo $RUN_ENV_FILE not readable
    exit 1
fi
. $RUN_ENV_FILE

assert_var_non_empty IMAGE

shift 2

LOG_OPTS=${LOG_OPTS:-"--log-driver journald --log-opt tag=$_SERVICE"}
CGROUP_PARENT_OPTS=${CGROUP_PARENT_OPTS:-"--cgroup-parent=$CGROUP_PARENT"}

if [ z"$PULL" != z0 ]; then
    /usr/bin/docker pull ${IMAGE}
fi
exec /usr/bin/docker run -d --name $_NAME ${LOG_OPTS} ${CGROUP_PARENT_OPTS} ${RUN_OPTS} ${IMAGE} "${CMD[@]}"
